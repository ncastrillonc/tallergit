<?php

require('configs/include.php');

class c_modificarprofesor extends super_controller {
    
    public function actualizar()
    {     
        function verificarID($objeto, $oldID, $identificacion)
        {
            $objeto->orm->connect();

            $cod['profesor']['cedula'] = $identificacion;
            $options['profesor']['lvl2']="count_by_ced";
            $objeto->orm->read_data(array("profesor"),$options,$cod);
            
            $resultado = $objeto->orm->data;
            $contador = $resultado['profesor'][0];
            
            $objeto->orm->close();
        
            if($contador->contador ==  1 && $oldID != $identificacion)
            {
                return 1;
            } else {
                return 0;
            }
        }
        
        $profesor = new profesor($this->post);
        
        if(is_empty($profesor->get('cedula')))
        { 
            throw_exception("Debe ingresar una cedula"); 
        }  
        
        $id_old = $this->post->auxiliar;
        $profesor->auxiliars['id_old'] = $id_old;
        
        if(verificarID($this, $id_old, $profesor->get('cedula')) == 1)
        { 
            throw_exception("El profesor con cedula " . $profesor->get('cedula') . " ya se encuentra registrado"); 
        }
        
        $this->orm->connect();
        $this->orm->update_data("normal",$profesor);
        $this->orm->close();         
        
        $this->type_warning = "success";
        $this->msg_warning = "Profesor actualizado correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning',$this->type_warning);
        $this->engine->assign('msg_warning',$this->msg_warning);
    }
    
    public function buscar()
    {
        function hallarID($objeto, $Ide)
        {
            $objeto->orm->connect();

            $cod['profesor']['cedula'] = $Ide;
            $options['profesor']['lvl2']="count_by_ced";
            $objeto->orm->read_data(array("profesor"),$options,$cod);
            
            $resultado = $objeto->orm->data;
            $contador = $resultado['profesor'][0];
            
            $objeto->orm->close();
        
            if($contador->contador ==  1)
            {
                return 1;
            } else {
                return 0;
            }
        }
        
        $prof = new profesor($this->post);
        
        if(is_empty($prof->get('cedula')))
        {
            throw_exception("Debe ingresar una cedula");
        }
        else if(hallarID($this, $prof->get('cedula')) == 0)
        {
            throw_exception("El Profesor con cedula = " . $prof->get('cedula') . " no existe");
        }
        else
        {
            $this->orm->connect();

            $cod['profesor']['cedula'] = $_POST['cedula'];
            $options['profesor']['lvl2']="by_cedula";
            $this->orm->read_data(array("profesor"),$options,$cod);
            $profesor = $this->orm->get_objects("profesor");
            
            $this->orm->close();
            $this->engine->assign('profesor', $profesor[0]);   
        }
    }

    public function display()
    {
        $profe = new profesor($this->post);
        
        $this->engine->assign('title','Modificar Profesor');
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        
        if(is_empty($profe->get('cedula')) || !is_empty($profe->get('nombre'))){
            $this->engine->display('buscarced.tpl');
        } else{
            if(!is_empty($this->orm->objects_to_return)){
                $this->engine->display('modificarprofesor.tpl');
            } else{
                $this->engine->display('buscarced.tpl');
            }
            
        }        
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {
            if (isset($this->get->option))
            {
                $this->{$this->get->option}();
            }
        }
        catch (Exception $e) 
        {
            $this->error=1; $this->msg_warning=$e->getMessage();
            $this->engine->assign('type_warning',$this->type_warning);
            $this->engine->assign('msg_warning',$this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }    
        $this->display();
    }
}

$call = new c_modificarprofesor();
$call->run();

?>
