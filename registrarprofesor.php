<?php

require('configs/include.php');

class c_registrarprofesor extends super_controller {

    public function add()
    {
        $profe = new profesor($this->post);
        
        if(is_empty($profe->get('cedula')))
        {
            throw_exception("Debe ingresar una cedula");
        }
		
        $this->orm->connect();
        $this->orm->insert_data("normal",$profe);
        
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Profesor registrado correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning',$this->type_warning);
        $this->engine->assign('msg_warning',$this->msg_warning);
    }

    public function display()
    {
        $this->engine->assign('title','registrar Profesor');
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('registrarprofesor.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {
            if (isset($this->get->option))
            {
                $this->{$this->get->option}();
            }
        }
        catch (Exception $e) 
        {
            $this->error=1; $this->msg_warning=$e->getMessage();
            $this->engine->assign('type_warning',$this->type_warning);
            $this->engine->assign('msg_warning',$this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }    
        $this->display();
    }
}

$call = new c_registrarprofesor();
$call->run();

?>
