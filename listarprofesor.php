<?php

require('configs/include.php');

class c_listarprofesor extends super_controller {

    public function display()
    {	
        $options['profesor']['lvl2']="all";

        $this->orm->connect();
        $this->orm->read_data(array("profesor"),$options);
        $profesor = $this->orm->get_objects("profesor");
        $this->orm->close();
        
        $this->engine->assign('profesor',$profesor);

        $this->engine->assign('title','Listar Profesores');
        $this->engine->display('header.tpl');
        $this->engine->display('listarprofesor.tpl');
        $this->engine->display('footer.tpl');
    }

    public function run()
    {
            $this->display();
    }
}

$call = new c_listarprofesor();
$call->run();

?>